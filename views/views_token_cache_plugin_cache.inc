<?php

/**
 * @file
 * Definition of views_token_cache_plugin_cache.
 */

/**
 * Simple caching with the cache id (cid) set with help from Token
 */
class views_token_cache_plugin_cache extends views_plugin_cache {
  
  /**
   * Override of parent::get_results_key()
   */
  function get_results_key() {
    if (!isset($this->_results_key)) {
      // Get the "key data" from parent class.
      $key_elements = explode(':', parent::get_results_key());
      $this->_results_key = token_replace($this->options['cache_key'], array('view' => $this->view)) . ':result:' . $key_elements[3];
    }
    return $this->_results_key;
  }

  /**
   * Override of parent::get_output_key()
   */
  function get_output_key() {
    if (!isset($this->_output_key)) {
      // Get the "key data" from parent class.
      $key_elements = explode(':', parent::get_output_key());
      $this->_output_key = token_replace($this->options['cache_key'], array('view' => $this->view)) . ':output:' . $key_elements[3];
    }
    return $this->_output_key;
  }
  
  /**
   * Return a short title for this cache plugin
   */
  function summary_title() {
    return t('Token settings');
  }
  
  function option_definition() {
    $options = parent::option_definition();
    $options['cache_key'] = array('default' => '');
    return $options;
  }

  function option_defaults(&$options) {
    $options['cache_key'] = '';
  }

  function options_form(&$form, &$form_state) {
    $form['cache_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Token CID'),
      '#description' => t('The "key data" (md5 hashed) from Views default cache cid will be appended.'),
      '#default_value' => $this->options['cache_key'],
    );

    if (module_exists("token")) {
      $form['token_help']['content'] = array(
        '#type' => 'markup',
        '#token_types' => array('view'),
        '#theme' => 'token_tree'
      );
    }
  }
}
