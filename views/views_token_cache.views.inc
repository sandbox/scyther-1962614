<?php

/**
 * @file
 * Cache plugin for Views. Uses Tokens in the cache id (cid).
 */

/**
 * Implements hook_views_plugins().
 */
function views_token_cache_views_plugins() {
  return array(
    'cache' => array(
      'views_token_cache' => array(
        'path' => drupal_get_path('module', 'views_token_cache') . '/views',
        'title' => t('Token-based'),
        'help' => t('Simple caching using cache id based on tokens.'),
        'handler' => 'views_token_cache_plugin_cache',
        'uses options' => TRUE,
      ),
    ),
  );
}
